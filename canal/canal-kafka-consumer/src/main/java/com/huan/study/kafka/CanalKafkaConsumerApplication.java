package com.huan.study.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan
 */
@SpringBootApplication
public class CanalKafkaConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CanalKafkaConsumerApplication.class, args);
    }

}
