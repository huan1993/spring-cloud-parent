-- 滑动窗口lua 脚本
-- 该脚本未进行测试
-- 不可使用 local 变量，在集群模式下会有问题
-- 在lua脚本中只可使用 KEYS 数据，不可直接写死key。
-- 在lua集群中，传递给KEYS数组的key，需要带上hash标签，这样所有的key才可以在同一个hash槽 比如： {user:admin}:ip {user:admin}就是一个hash标签
-- KEYS[1]: Redis键，用于存储滑动窗口的请求时间戳
-- ARGV[1]: 当前时间戳
-- ARGV[2]: 限流阈值
-- ARGV[3]: 窗口大小（单位：秒）
-- ARGV[4]: 元素对应的值

-- 移除窗口开始之前的所有请求
redis.call('ZREMRANGEBYSCORE', KEYS[1], 0, tonumber(ARGV[1]) - tonumber(ARGV[3]))
-- 统计窗口开始时间之前的所有请求记录
-- 判断是否超过阈值
if tonumber(redis.call('ZCARD', KEYS[1]) or '1') >= tonumber(ARGV[2]) then
    -- 超过直接返回 false
    return false
else
    -- 没有超过，添加到当前窗口中
    redis.call('ZADD', KEYS[1], tonumber(ARGV[1]) + 0.0, ARGV[4])
    -- 设置过期时间
    redis.call('expire', KEYS[1], 3)
    -- 返回true，表示添加成功
    return true
end




