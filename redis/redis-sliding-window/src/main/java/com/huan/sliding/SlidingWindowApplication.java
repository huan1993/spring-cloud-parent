package com.huan.sliding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;

/**
 * 滑动窗口启动类
 *
 * @author huan
 */
@SpringBootApplication
public class SlidingWindowApplication {
    public static void main(String[] args) throws IOException {
        SpringApplication.run(SlidingWindowApplication.class, args);
    }
}
