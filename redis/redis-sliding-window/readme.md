# 1、实现功能

基于`redis`的`zset` 实现一个滑动窗口案例。

# 2、zset 的基本用法

`zset`是redis中的有序集合，它里面的每个元素都不可重复，并且每个元素都关联一个分数。这个分数用于对集合中的元素进行排序。

1. `zadd key score member`: 将元素member及其分数score添加到有序集合key中。
2. `ZRANGEBYSCORE key min max [WITHSCORES] [LIMIT offset count]`: 返回有序集合 key 中，分数在 min 和 max 之间的元素。如果使用 WITHSCORES 选项，还会返回每个元素的分数。LIMIT 选项用于限制返回的元素数量。(包括min和max)
3. `ZCARD key`: 返回有序集合 key 中的元素数量。
4. `ZREMRANGEBYSCORE key min max`: 移除有序集合 key 中分数在 min 和 max 之间的所有元素。（包括min和max）

# 3、滑动窗口实现的步骤

1. 定义滑动窗口的大小，比如： 5s，并确定窗口周期内，只可允许多少个请求，比如：10s
2. 删除窗口外的数据（当前时间 - 窗口大小）
3. 判断窗口内的数据是否超过了限制大小
4. 添加元素（需要：zset 中的member的值不可一样）


