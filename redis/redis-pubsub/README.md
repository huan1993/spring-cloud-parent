# 需求

学习redis 的发布订阅

# 解释

`ChannelTopic`: 直接匹配
`PatternTopic`: 模糊匹配
&nbsp;&nbsp;&nbsp;`*`:  表示匹配0个或多个字符
&nbsp;&nbsp;&nbsp;`?`:  表示匹配1个字符
&nbsp;&nbsp;&nbsp;`?*`: 表示匹配1个或1个以上的字符