package com.huan.study.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan
 */
@SpringBootApplication
public class RedisIdApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(RedisIdApplication.class, args);
    }
}
