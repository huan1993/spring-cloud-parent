package com.huan.flink.source.collection;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;

/**
 * flink 从 集合中读取数据，一般用于测试
 *
 * @author huan.fu
 * @date 2023/9/17 - 22:45
 */
public class FlinkCollectionSourceApplication {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment environment = StreamExecutionEnvironment.getExecutionEnvironment();
        // 设置默认的并行度为1
        environment.setParallelism(1);

        /**
         * 从集合中读取数据
         */
        // environment.fromElements("hello", "java", "flink")
        environment.fromCollection(Arrays.asList("hello", "java", "flink"))
                // 数据拼接
                .map((MapFunction<String, String>) str -> str + " --> " + str)
                // 设置返回类型
                .returns(Types.STRING)
                // 打印
                .print();

        environment.execute("read-from-collection-source");
    }

}
