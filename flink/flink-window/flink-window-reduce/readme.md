# 需求

每10s输出，同一类型商品最后的一次创建时间。

# 实现步骤

1. 使用 滚动 窗口来进行统计，窗口大小为 10s
2. 使用 reduce 来进行增量聚合计算

# reduce 注意事项

1. 在一个窗口周期内：相同 key 的第一条数据来的时候，不会调用reduce方法。
2. reduce 可以解决大多数归约聚合的问题，但是这个接口有一个限制，就是聚合状态的类型、输出结果的类型都必须和输入数据类型一样。

# 启动scoket服务

```shell
➜  ~ nc -lk 9999
1,苹果1,2024-01-06 00:00:01
1,苹果1,2024-01-06 00:00:02
```