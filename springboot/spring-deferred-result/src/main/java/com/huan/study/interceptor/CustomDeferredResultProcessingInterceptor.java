package com.huan.study.interceptor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.async.DeferredResult;
import org.springframework.web.context.request.async.DeferredResultProcessingInterceptor;

/**
 * @author huan.fu 2021/10/14 - 上午10:51
 */
@Component
public class CustomDeferredResultProcessingInterceptor implements DeferredResultProcessingInterceptor {
    
    private static final Logger log = LoggerFactory.getLogger(CustomDeferredResultProcessingInterceptor.class);
    
    @Override
    public <T> void beforeConcurrentHandling(NativeWebRequest request, DeferredResult<T> deferredResult) throws Exception {
        log.info("beforeConcurrentHandling");
    }
    
    @Override
    public <T> void preProcess(NativeWebRequest request, DeferredResult<T> deferredResult) throws Exception {
        log.info("preProcess");
    }
    
    @Override
    public <T> void postProcess(NativeWebRequest request, DeferredResult<T> deferredResult, Object concurrentResult) throws Exception {
        log.info("postProcess");
    }
    
    /**
     * 返回 false ,则不会在此进入下个拦截器进行处理
     */
    @Override
    public <T> boolean handleTimeout(NativeWebRequest request, DeferredResult<T> deferredResult) throws Exception {
        log.info("handleTimeout");
        return true;
    }
    
    /**
     * 返回 false ,则不会在此进入下个拦截器进行处理
     */
    @Override
    public <T> boolean handleError(NativeWebRequest request, DeferredResult<T> deferredResult, Throwable t) throws Exception {
        log.info("handleError");
        return true;
    }
    
    @Override
    public <T> void afterCompletion(NativeWebRequest request, DeferredResult<T> deferredResult) throws Exception {
        log.info("-->afterCompletion");
    }
}
