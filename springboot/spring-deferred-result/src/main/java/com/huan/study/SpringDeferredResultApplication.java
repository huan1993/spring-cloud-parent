package com.huan.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class SpringDeferredResultApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(SpringDeferredResultApplication.class, args);
    }
}
