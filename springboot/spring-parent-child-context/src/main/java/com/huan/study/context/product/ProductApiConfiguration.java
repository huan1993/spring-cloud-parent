package com.huan.study.context.product;

import com.huan.study.context.CommonApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author huan.fu 2021/6/8 - 下午2:27
 */
@Configuration
public class ProductApiConfiguration {
    //@Bean
    public CommonApi commonApi() {
        return new ProductApi();
    }
}
