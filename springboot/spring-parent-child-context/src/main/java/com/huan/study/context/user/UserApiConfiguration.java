package com.huan.study.context.user;

import com.huan.study.context.CommonApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author huan.fu 2021/6/8 - 下午2:26
 */
@Configuration
public class UserApiConfiguration {

    @Bean
    public CommonApi commonApi() {
        return new UserApi();
    }
}
