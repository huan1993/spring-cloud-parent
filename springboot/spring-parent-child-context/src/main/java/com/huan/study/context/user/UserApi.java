package com.huan.study.context.user;

import com.huan.study.context.CommonApi;

/**
 * @author huan.fu 2021/6/8 - 下午2:24
 */
public class UserApi implements CommonApi {
    @Override
    public String apiName() {
        return "user - api";
    }
}
