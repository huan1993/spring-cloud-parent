package com.huan.springboot.evaluator;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.boolex.EvaluationException;
import ch.qos.logback.core.boolex.EventEvaluatorBase;

/**
 * 判断一整个异常堆栈是否需要输出
 *
 * @author huan.fu
 * @date 2022/5/7 - 15:32
 */
public class CustomEventEvaluator extends EventEvaluatorBase<ILoggingEvent> {

    public CustomEventEvaluator() {
        setName("customEventEvaluator");
    }

    @Override
    public boolean evaluate(ILoggingEvent event) throws NullPointerException, EvaluationException {



        return true;
    }
}
