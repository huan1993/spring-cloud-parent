package com.huan.h2.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * 产品实体类
 *
 * @author huan.fu
 * @date 2025/2/5 - 15:17
 */
@TableName(value = "product")
public class Product {
    /**
     * '商品编号'
     */
    @TableId(type = IdType.NONE)
    private String productId;
    /**
     * '商品名称'
     */
    private String productName;
    /**
     * '商品价格'
     */
    private String productPrice;
    /**
     * 状态
     */
    private Integer status;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(String productPrice) {
        this.productPrice = productPrice;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Product{" +
                "productId='" + productId + '\'' +
                ", productName='" + productName + '\'' +
                ", productPrice='" + productPrice + '\'' +
                ", status=" + status +
                '}';
    }
}
