package com.huan.h2.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huan.h2.entity.Product;

/**
 * product mapper
 *
 * @author huan.fu
 * @date 2025/2/5 - 15:20
 */
public interface ProductMapper extends BaseMapper<Product> {

}
