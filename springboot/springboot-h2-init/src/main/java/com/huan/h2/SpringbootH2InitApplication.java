package com.huan.h2;

import com.huan.h2.entity.Product;
import com.huan.h2.mapper.ProductMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.Resource;

/**
 * 启动类
 */
@SpringBootApplication
@MapperScan(basePackages = "com.huan.h2.mapper")
public class SpringbootH2InitApplication implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(SpringbootH2InitApplication.class);

    @Resource
    private ProductMapper productMapper;

    public static void main(String[] args) {
        SpringApplication.run(SpringbootH2InitApplication.class, args);
    }


    @Override
    public void run(String... args) throws Exception {
        for (Product product : productMapper.selectList(null)) {
            log.info("{}", product);
        }
    }
}
