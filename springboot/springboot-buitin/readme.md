# 功能介绍

## 打印请求数据，比如：请求头、请求参数、客户端信息等

1. 配置CommonsRequestLoggingFilter

```java
@Configuration
public class RequestLoggingConfiguration {
    @Bean
    public CommonsRequestLoggingFilter commonsRequestLoggingFilter() {
        CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
        filter.setIncludeClientInfo(true);
        filter.setIncludeHeaders(true);
        filter.setIncludePayload(true);
        filter.setIncludeQueryString(true);
        filter.setBeforeMessagePrefix("==> Before request [");
        filter.setBeforeMessageSuffix("]");
        return filter;
    }
}
```

2. 配置日志级别

```properties
logging.level.org.springframework.web.filter.CommonsRequestLoggingFilter=debug
```

3. 测试

```shell
curl --location --request POST 'http://localhost:8080/printRequestInfo?aa=bb' \
--header 'token: aaaaaa' \
--header 'User-Agent: Apifox/1.0.0 (https://apifox.com)' \
--header 'Content-Type: application/json' \
--data-raw '{
  "username": "张三",
  "sex": 1
}'
```