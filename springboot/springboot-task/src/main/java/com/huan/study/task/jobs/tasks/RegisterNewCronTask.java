package com.huan.study.task.jobs.tasks;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;
import org.springframework.stereotype.Component;

/**
 * 程序启动后，动态的注册一个定时任务
 *
 * @author huan.fu
 * @date 2023/3/27 - 00:05
 */
@Slf4j
@Component
public class RegisterNewCronTask implements SchedulingConfigurer {

    @Override
    public void configureTasks(ScheduledTaskRegistrar registrar) {
        registrar.addCronTask(() -> log.info("程序启动后,动态添加的一个cron任务"), "* * * * * *");
    }
}
