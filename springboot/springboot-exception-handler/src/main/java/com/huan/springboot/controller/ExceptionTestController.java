package com.huan.springboot.controller;

import com.huan.springboot.exception.BizException;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

/**
 * 测试异常
 *
 * @author huan.fu
 * @date 2022/4/27 - 20:55
 */
@RestController
@RequestMapping("exception")
public class ExceptionTestController {

    static class Req {
        @NotBlank
        public String password;
    }

    @PostMapping("password")
    public String checkPassword(@Validated @RequestBody Req req) {

        if (Objects.equals(req.password, "exception")) {
            throw new BizException("密码传递的是exception字符串");
        }

        return "当前密码,password: " + req.password;
    }
}
