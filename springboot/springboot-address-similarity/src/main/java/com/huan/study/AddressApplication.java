package com.huan.study;

import org.bitlap.geocoding.Geocoding;
import org.bitlap.geocoding.model.Address;
import org.bitlap.geocoding.similarity.Document;
import org.bitlap.geocoding.similarity.MatchedResult;

/**
 * 地址解析相关
 */
public class AddressApplication {
    public static void main(String[] args) {

        // 比较地址的相似度
        double similarity = Geocoding.similarity("湖北黄冈市罗田县凤山镇", "湖北省黄冈市罗田县凤山镇");
        System.out.println(similarity);

        // 相似度计算，返回更多的信息
        MatchedResult matchedResult = Geocoding.similarityWithResult("湖北黄冈市罗田县凤山镇", "湖北省黄冈市罗田县凤山镇");
        System.out.println(matchedResult.toString());

        // 地址标准化
        Address address = Geocoding.normalizing("湖北黄冈市罗田县凤山镇");
        System.out.printf(address.toString());

        // 解析成分词文档
        Document document = Geocoding.analyze("山东青岛市北区山东省青岛市市北区水清沟街道九江路20号大都会3号楼2单元1303");
        System.out.println(document.toString());
    }
}
