# 1、功能介绍

1. normalizing: 地址标准化
2. analyze: 地址解析成分词文档
3. similarity: 地址相似度计算
4. similarityWithResult: 地址相似度计算, 返回包含更多丰富的数据

# 2、主要使用到的类

`org.bitlap.geocoding.Geocoding

# 3、参考文档

[https://github.com/bitlap/geocoding?tab=readme-ov-file](https://github.com/bitlap/geocoding?tab=readme-ov-file)

