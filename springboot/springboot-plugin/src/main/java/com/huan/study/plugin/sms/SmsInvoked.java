package com.huan.study.plugin.sms;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.stereotype.Component;

/**
 * @author huan.fu 2021/10/22 - 下午3:56
 */
@Component
public class SmsInvoked implements InitializingBean {
    
    @Autowired
    private PluginRegistry<SmsPlugin, SmsType> pluginRegistry;
    
    public void invoked(SmsType smsType, String phone, String content) {
        SmsPlugin smsPlugin = pluginRegistry.getRequiredPluginFor(smsType);
        smsPlugin.sendSms(phone, content);
    }
    
    @Override
    public void afterPropertiesSet() {
        for (SmsPlugin plugin : pluginRegistry.getPlugins()) {
            System.out.println(plugin.getClass().getName());
        }
        invoked(SmsType.ALIYUN, "18251422048", "您的短信验证码是:123456");
        invoked(SmsType.TENXUNYUN, "18251422049", "您的短信验证码是:654321");
    }
}
