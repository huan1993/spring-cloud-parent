# Spring Plugin 的使用 

Spring Plugin 的使用。

## 场景

在实际的业务场景中，我们有发送短信的例子，但是短信的提供可能存在多个渠道进行发送，比如：`阿里云`或`腾讯云`等等。
那么这种情况下我们就可以提供一个策略根据不同的场景进行短信的发送。在Spring的体系中有一个`Spring-Plugin`工程
正好可以帮助我们来实现这个功能。

## 实现的例子

1. 通过不同的渠道发送短信

## 实现步骤

> 此处通过以不同的渠道来发送短信为例
>
> 如果需要指定顺序则使用 @Order 注解来实现

### 0、引入插件

```xml
<dependency>
    <groupId>org.springframework.plugin</groupId>
    <artifactId>spring-plugin-core</artifactId>
    <version>2.0.0.RELEASE</version>
</dependency>
```

### 1、编写一个类集成Plugin接口

```java
public interface SmsPlugin extends Plugin<SmsType> {
    /**
     * 发送短信
     *
     * @param phone   手机号
     * @param content 短信内容
     */
    void sendSms(String phone, String content);
}
```

### 2、编写具体具体的实现类

```java
/**
 * 阿里云渠道发送短信
 *
 * @author huan.fu 2021/10/22 - 下午3:45
 */
@Component
public class AliyunSmsPluginProvider implements SmsPlugin {
    
    private static final Logger log = LoggerFactory.getLogger(AliyunSmsPluginProvider.class);
    
    @Override
    public boolean supports(SmsType smsType) {
        return smsType == SmsType.ALIYUN;
    }
    
    @Override
    public void sendSms(String phone, String content) {
        log.info("通过阿里云渠道 给phone:[{}]发送短信:[{}]成功", phone, content);
    }
}
```

### 3、注册到Spring上

```java
@SpringBootApplication
// value 写接口即可
@EnablePluginRegistries(value = {SmsPlugin.class})
public class SpringbootPluginApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(SpringbootPluginApplication.class, args);
    }
}
```

### 4、使用

```java
@Component
public class SmsInvoked implements InitializingBean {
    
    @Autowired
    private PluginRegistry<SmsPlugin, SmsType> pluginRegistry;
    
    public void invoked(SmsType smsType, String phone, String content) {
        SmsPlugin smsPlugin = pluginRegistry.getRequiredPluginFor(smsType);
        smsPlugin.sendSms(phone, content);
    }
    
    @Override
    public void afterPropertiesSet() throws Exception {
        invoked(SmsType.ALIYUN, "18251422048", "您的短信验证码是:123456");
        invoked(SmsType.TENXUNYUN, "18251422049", "您的短信验证码是:654321");
    }
}
```

## 参考链接

1. [https://github.com/spring-projects/spring-plugin](https://github.com/spring-projects/spring-plugin)