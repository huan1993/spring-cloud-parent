package com.huan.gzip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 测试SpringBoot程序中响应增加Gzip压缩
 *
 * @author huan
 */
@SpringBootApplication
public class GzipApplication {
    public static void main(String[] args) {
        SpringApplication.run(GzipApplication.class, args);
    }
}
