package com.huan.study;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LogstashGrokApplication {

    public static void main(String[] args) {
        SpringApplication.run(LogstashGrokApplication.class, args);
    }
}
