# 1、背景
在我们的日常开发中，经常会存在在一个Service层中调用另外一个Service层的方法。`比如`：我们有一个TaskService，里面有一个execTask方法，且这个方法存在事物，这个方法在执行完之后，需要调用LogService的insertLog方法记录一条日志，这个方法上也有事物，不管日志记录成功还是失败，都不能影响execTask方法的执行。因此我们很容易写出如下代码。
```java
@Transactional
public void execTaskV1(){
    log.info("开始执行任务");
    try {
        logService.insertLogV1();
    } catch (Exception e) {
        log.error("添加日志出现错误");
    }
    log.info("完成任务执行");
}
```
`思考：` 上方的代码，如果`insertLogV1`跑出了异常，`execTaskV1`方法的事物可以正常提交吗？

# 2、异常是如何实现出现的
## 1、了解Spring事物的传播属性
以下是使用 Markdown 语法编写的 Spring 事务传播属性的表格：
markdown复制
| **传播行为**          | **描述**                                                                 | **应用场景**                                                                 | **行为特点**                                                                 |
|:----------------------|:-------------------------------------------------------------------------|:-----------------------------------------------------------------------------|:-----------------------------------------------------------------------------|
| `Propagation.REQUIRED` | 如果当前存在事务，则加入该事务；如果当前没有事务，则启动一个新的事务。   | 大多数场景，如多个方法需要在同一个事务中完成。                               | - 如果当前事务存在，方法执行在当前事务上下文中。<br>- 如果当前事务不存在，创建新事务。 |
| `Propagation.SUPPORTS` | 如果当前存在事务，则加入该事务；如果当前没有事务，则以非事务方式执行。   | 对事务支持没有强制要求的场景，如只读查询。                                    | - 如果当前事务存在，方法执行在当前事务上下文中。<br>- 如果当前事务不存在，以非事务方式执行。 |
| `Propagation.MANDATORY`| 如果当前存在事务，则加入该事务；如果当前没有事务，则抛出异常。           | 必须在一个已存在的事务中执行的场景。                                         | - 必须在已有事务中执行，否则抛出 `IllegalTransactionStateException`。       |
| `Propagation.REQUIRES_NEW`| 每次调用该方法时都会启动一个新的事务。当前事务（如果有）会被挂起。       | 需要独立事务的场景，如日志记录或独立的业务操作。                               | - 总是创建新事务。<br>- 当前事务（如果有）会被挂起，直到新事务完成。         |
| `Propagation.NOT_SUPPORTED`| 总是以非事务方式执行，并且暂停当前事务（如果有）。                       | 不需要事务的场景，如简单的查询操作。                                           | - 总是以非事务方式执行。<br>- 暂停当前事务（如果有）。                       |
| `Propagation.NEVER`    | 总是以非事务方式执行，如果当前存在事务，则抛出异常。                     | 严格禁止事务的场景，如某些非事务性操作。                                       | - 必须在非事务上下文中执行，否则抛出 `TransactionException`。               |
| `Propagation.NESTED`   | 如果当前存在事务，则在嵌套事务内执行；如果当前没有事务，则启动一个新的事务。| 需要嵌套事务的场景，如复杂的业务流程中需要独立的回滚点。                       | - 如果当前事务存在，创建一个嵌套事务（依赖于数据库支持）。<br>- 如果当前事务不存在，创建新事务。 |

## 2、模拟异常出现
`Transaction rolled back because it has been marked as rollback-only` 这个异常在上述的案例中是如何实现的呢？

![异常出现](img.png)

从上图中可知，出现了`Transaction rolled back because it has been marked as rollback-only`这个异常，那么这个异常是如何出现的呢？

其实这个是和Spring事物的`传播属性Propagation`有关。

默认情况下`@Transaction`的传播属性是`Propagation.REQUIRED`, 即`如果当前存在事务，则加入该事务；如果当前没有事务，则启动一个新的事务。` 在我们的例子中，事物的隔离级别都是`Propagation.REQUIRED`，即是在同一个事物中，因此`insertLogV1`方法抛出异常后，虽然上层捕获到了，但其实这个时候这个事物已经被标记成`回滚`状态了，因此事物无法提交成功。

`如何解决：`  只需要修改`insertLogV1`事物的传播属性为`Propagation.REQUIRES_NEW`即可。

# 3、api调用

1. 出现异常 http://localhost:8080/transaction/execTaskV1
2. 不出现异常 http://localhost:8080/transaction/execTaskV2