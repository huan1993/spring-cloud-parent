package com.huan.transaction.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 丁达
 * @author huan.fu
 * @date 2025/2/5 - 13:09
 */
@Service
public class TaskServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);

    private final LogServiceImpl logService;
    public TaskServiceImpl(LogServiceImpl logService){
        this.logService=logService;
    }


    @Transactional
    public void execTaskV1(){
        log.info("开始执行任务");
        try {
            logService.insertLogV1();
        } catch (Exception e) {
            log.error("添加日志出现错误");
        }
        log.info("完成任务执行");
    }
    @Transactional
    public void execTaskV2(){
        log.info("开始执行任务");
        try {
            logService.insertLogV2();
        } catch (Exception e) {
            log.error("添加日志出现错误");
        }
        log.info("完成任务执行");
    }
}
