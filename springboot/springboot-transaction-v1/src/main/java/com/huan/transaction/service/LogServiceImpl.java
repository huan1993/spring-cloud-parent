package com.huan.transaction.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * 日志服务类
 *
 * @author huan.fu
 * @date 2025/2/5 - 13:09
 */
@Service
public class LogServiceImpl {

    private static final Logger log = LoggerFactory.getLogger(LogServiceImpl.class);


    @Transactional
    public void insertLogV1() {
        log.info("添加一行日志");
        int i = 1 / 0;
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void insertLogV2() {
        log.info("添加一行日志");
        int i = 1 / 0;
    }
}
