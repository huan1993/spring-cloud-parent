package com.huan.transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 验证SpringBoot 的事物， A和B方法同时开启事物，A方法中调用B方法，在A方法中捕获B方法的异常
 */
@SpringBootApplication
public class SpringbootTransactionV1Application {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootTransactionV1Application.class, args);
    }

}
