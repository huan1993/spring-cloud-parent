package com.huan.transaction.controller;

import com.huan.transaction.service.TaskServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 控制器
 *
 * @author huan.fu
 * @date 2025/2/5 - 13:09
 */
@Controller
@RequestMapping("transaction")
public class TransactionController {

    private final TaskServiceImpl taskService;

    public TransactionController(TaskServiceImpl taskService) {
        this.taskService = taskService;
    }

    /**
     * 事物会回滚且出现Transaction rolled back because it has been marked as rollback-only异常
     */
    @GetMapping("execTaskV1")
    public String execTaskV1() {
        taskService.execTaskV1();
        return "执行成功";
    }

    /**
     * 事物正常提交
     */
    @GetMapping("execTaskV2")
    public String execTaskV2() {
        taskService.execTaskV2();
        return "执行成功";
    }
}
