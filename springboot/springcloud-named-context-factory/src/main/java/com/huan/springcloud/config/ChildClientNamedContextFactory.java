package com.huan.springcloud.config;

import org.springframework.cloud.context.named.NamedContextFactory;

/**
 * 自定义客户端 父子上下文
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:35
 */
public class ChildClientNamedContextFactory extends NamedContextFactory<ClientSpecification> {
    /**
     * {@link NamedContextFactory#createContext(String)}
     */
    public static final String NAMESPACE = "loadbalancer";
    /**
     * {@link NamedContextFactory#createContext(String)}
     */
    public static final String PROPERTY_NAME = "loadbalancer.client.name";

    public ChildClientNamedContextFactory(Class<?> defaultConfigType) {
        super(defaultConfigType, NAMESPACE, PROPERTY_NAME);
    }
}
