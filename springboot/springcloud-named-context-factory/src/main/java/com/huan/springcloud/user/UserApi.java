package com.huan.springcloud.user;

import com.huan.springcloud.common.CommonApi;
import com.huan.springcloud.config.ChildClientNamedContextFactory;
import org.springframework.core.env.Environment;

import javax.annotation.Resource;

/**
 * user api
 *
 * @author huan.fu
 * @date 2023/8/15 - 09:58
 */
public class UserApi implements CommonApi {
    @Resource
    private Environment environment;

    @Override
    public void showName() {
        System.out.println("product-api" + ": " + environment.getProperty(ChildClientNamedContextFactory.PROPERTY_NAME));
    }
}
