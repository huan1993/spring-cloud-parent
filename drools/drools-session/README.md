# 功能
1、完成drools有状态和无状态session的理解  
2、session级别全局变量的使用  
3、session 线程安全的控制，默认是线程安全，可以修改成非线程安全

# 案例想表达的内容

分2步向drools的工作内存中插入Count对象，看看在有状态和无状态的情况下，之前一步插入的对象是否存在

# session 级别全局变量的使用

```
StatelessKieSession session = kieClasspathContainer.newStatelessKieSession("stateless-session");

// 设置一个session级别的全局变量
session.setGlobal("username", "huan.fu");
```

```drl
// 引入全局变量
global java.lang.String username
```

# 博客地址
[https://blog.csdn.net/fu_huo_1993/article/details/124751731](https://blog.csdn.net/fu_huo_1993/article/details/124751731)