package com.huan.drools;

import org.drools.core.audit.WorkingMemoryInMemoryLogger;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.StatelessKieSession;

/**
 * drools 测试类
 */
public class DroolsApplication {
    public static void main(String[] args) {
        // 无状态session测试
        statelessSessionTest();
        // 有状态session测试
        // statefulSessionTest();
    }

    private static void statelessSessionTest() {
        // 获取kie services
        KieServices kieServices = KieServices.get();
        // 获取kie容器对象
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        // 获取kie session , 此处获取的是无状态的session，因为 <ksession name="shop-ksession" default="false" type="stateless"/>
        // 中type="stateless"就是无状态的session
        StatelessKieSession kieSession = kieContainer.newStatelessKieSession("shop-ksession");
        // 创建一个对象，可以理解为 Fact对象，即事实对象
        CustomerOrder customerOrder = new CustomerOrder(2);
        // 添加监听器，便于观察日志
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());
        kieSession.addEventListener(new DebugAgendaEventListener());
        // 无状态的session只需要执行 execute 方法即可。
        kieSession.execute(customerOrder);

        System.err.println("通过规则后，获取到的折扣为:" + customerOrder.getDiscount());
    }

    private static void statefulSessionTest() {
        // 获取kie services
        KieServices kieServices = KieServices.get();
        // 获取kie容器对象
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        // 获取kie session , 此处获取的是有状态的session
        KieSession kieSession = kieContainer.newKieSession("shop-ksession-stateful");
        // 创建一个对象，可以理解为 Fact对象，即事实对象
        CustomerOrder customerOrder = new CustomerOrder(3);
        // 添加监听器，便于观察日志
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());
        // 将customerOrder对象加入到工作内存中
        kieSession.insert(customerOrder);
        // 触发所有的规则，如果只想触发指定的规则，则使用fireAllRules(AgendaFilter agendaFilter)方法
        kieSession.fireAllRules();

        // 有状态的session一定需要调用dispose方法
        kieSession.dispose();

        System.err.println("通过规则后，获取到的折扣为:" + customerOrder.getDiscount());
    }
}
