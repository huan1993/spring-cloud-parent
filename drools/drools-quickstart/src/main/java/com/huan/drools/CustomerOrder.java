package com.huan.drools;

/**
 * 客户购买衣服的订单
 *
 * @author huan.fu
 * @date 2022/5/12 - 11:27
 */
public class CustomerOrder {
    /**
     * 购买了几件衣服
     */
    private Integer purchaseQuantity;
    /**
     * 最终打多少折
     */
    private Double discount;

    public CustomerOrder(Integer purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getPurchaseQuantity() {
        return purchaseQuantity;
    }

    public void setPurchaseQuantity(Integer purchaseQuantity) {
        this.purchaseQuantity = purchaseQuantity;
    }

    @Override
    public String toString() {
        return "CustomerOrder{" +
                "purchaseQuantity=" + purchaseQuantity +
                ", discount=" + discount +
                '}';
    }
}
