package com.huan.drools;

import lombok.Getter;
import lombok.Setter;

/**
 * 客户类
 *
 * @author huan.fu
 * @date 2022/5/18 - 15:56
 */
@Getter
@Setter
public class Customer {

    // 信用分
    private Integer score;
    // 是否是黑名单用户
    private Boolean blackListUser;
}
