package com.huan.drools.droolsvariable;

import com.huan.drools.Fire;
import org.drools.core.base.RuleNameStartsWithAgendaFilter;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools 变量的使用，在drl文件中使用
 */
public class DroolsDroolsVariableApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("then-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        kieSession.insert(new Fire());
        kieSession.fireAllRules(new RuleNameStartsWithAgendaFilter("drools_"));

        kieSession.dispose();
    }
}
