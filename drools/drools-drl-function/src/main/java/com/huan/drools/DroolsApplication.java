package com.huan.drools;

import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools 测试类
 */
public class DroolsApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("function-ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        Person person = new Person("张三", 16);

        kieSession.insert(person);

        kieSession.fireAllRules();

        kieSession.dispose();
    }
}
