package com.huan.drools;

import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.EntryPoint;
import org.kie.soup.commons.util.Maps;

import java.util.Arrays;
import java.util.Date;

/**
 * drools when的写法
 */
public class DroolsWhenApplication {
    public static void main(String[] args) {
        // 设置drools中的日期格式
        System.setProperty("drools.dateformat", "yyyy-MM-dd HH:mm:ss");

        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());

        Person person1 = new Person("张三", 18, new Date(),
                Arrays.asList("打篮球", "打足球", "^张三"),
                new Maps.Builder<String, String>().put("key1", "value1").put("key2", "value2").build(),
                new BMWCar("宝马", "BMW"));
        kieSession.insert(person1);
        kieSession.insert(new Order(1001L, 10000L));

        // order-entry-point 这个是 drl 文件中定义的 查看 rule "entry-point" 这个
        EntryPoint entryPoint = kieSession.getEntryPoint("order-entry-point");
        entryPoint.insert(new Order(2001L, 10000L));

        kieSession.fireAllRules();

        kieSession.dispose();
    }
}
