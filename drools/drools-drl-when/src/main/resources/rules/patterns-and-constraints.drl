package rules

import com.huan.drools.com.huan.drools.fact.Person
import com.huan.drools.BMWCar
import java.text.SimpleDateFormat
import java.util.Date

// 总是执行 - 可以在下方规则执行前，完成一些环境的准备，比如：初始化一些数据
rule "总是执行"
    when
    then
        System.out.println("总是执行");
end

rule "工作内存中只要有Person对象就执行，存在多个执行多次"
    when Person()
    then
        System.out.println("工作内存中存在Person对象");
end

rule "只要工作内存中有对象，都会匹配到"
    when Object()
    then
        System.out.println("只要工作内存中有对象，都会匹配到");
end

rule "匹配年龄小于20岁的"
    when
        Person(age < 20) // 等驾与getAge() < 20，推荐使用属性的写法，条件中不可出现修改状态，比如getAge()方法中，修改了值错误的写法getAge(){ i++;return age }
    then
        System.out.println("匹配年龄小于20岁的");
end

rule "嵌套属性的访问-01"
    when
        Person(car.name == "宝马")
    then
        System.out.println("嵌套属性的访问-01");
end

rule "嵌套属性的访问-02"
    when
        Person( age < 20 && car.name == "宝马" && car.color == null)
    then
        System.out.println("嵌套属性的访问-02");
end

rule "嵌套属性的访问-03"
    when
        Person(age < 20 , car.(name == "宝马" || color != null)) // 属性分组访问
    then
        System.out.println("嵌套属性的访问-03");
end

rule "嵌套属性的访问-强制类型转换"
    when
        Person(age < 20 , car#BMWCar.name == "宝马") // 强制类型转换
    then
        System.out.println("嵌套属性的访问-强制类型转换");
end

rule "调用java方法约束"
    when
        Person(!isChild()) // isChild 方法中需要有状态的更改
    then
        System.out.println("调用java方法约束");
end

rule "多个字段约束-逗号"
    when
        Person(name != null , age < 20 , car != null) // isChild 方法中需要有状态的更改
    then
        System.out.println("多个字段约束-逗号");
end

rule "多个字段约束-&&"
    when
        Person((name != null && age < 20) && car != null) // isChild 方法中需要有状态的更改
    then
        System.out.println("多个字段约束-&&");
end

rule "使用绑定变量"
    when
        $p: Person($age: age)
    then
        System.err.println("使用绑定变量 " + $p.getName() + ": " + $age);
end

rule "使用绑定变量-不好的写法"
    when
        Person($age: age * 2 < 100)
    then
        System.err.println("使用绑定变量-不好的写法 " + ": " + $age);
end

rule "使用绑定变量-推荐的写法"
    when
        Person( age * 2 < 100, $age: age) // 这样写更清晰，运行效率更高
    then
        System.err.println("使用绑定变量-推荐的写法 " + ": " + $age);
end

rule "约束绑定只考虑后面的第一个原子表达式"
    when
        Person( $age: (age * 2)) // 此处 $age: (age * 2) 和 $age: age * 2 的结果是不一样的
    then
        System.err.println("约束绑定只考虑后面的第一个原子表达式 " + ": " + $age);
end

rule "日期类型的使用"
    when
        $p: Person(registerDate < '2022-05-24 12:12:12' ) // 日期格式比较，System.setProperty("drools.dateformat","yyyy-MM-dd HH:mm:ss");
    then
        System.err.println("日期类型的使用 注册时间:" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format($p.getRegisterDate()) );
end


rule ""
    when
    then
end