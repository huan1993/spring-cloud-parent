package com.huan.drools.agendafilter;

import com.huan.drools.Api;
import org.drools.core.event.DebugProcessEventListener;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.AgendaFilter;
import org.kie.api.runtime.rule.Match;

import java.util.Objects;

/**
 * drools 测试类
 */
public class DroolsAgendaFilterApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession = kieContainer.newKieSession("ksession");
        kieSession.addEventListener(new DebugRuleRuntimeEventListener());
        kieSession.addEventListener(new DebugAgendaEventListener());
        kieSession.addEventListener(new DebugProcessEventListener());

        Api api = new Api("/users/info", 100);
        kieSession.insert(api);

        // 所有模式匹配成功后的规则回进入到agenda中，然后通过AgendaFilter过滤出需要执行的规则
        kieSession.fireAllRules(new AgendaFilter() {
            @Override
            public boolean accept(Match match) {
                String ruleName = match.getRule().getName();
                return Objects.equals(ruleName, "rule_agenda_filter_02");
            }
        });

        kieSession.dispose();
    }
}
