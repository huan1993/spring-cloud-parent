package com.huan.drools;

import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * drools 测试类
 */
public class DroolsGlobalVariableApplication {
    public static void main(String[] args) {
        KieServices kieServices = KieServices.get();
        KieContainer kieContainer = kieServices.getKieClasspathContainer();
        KieSession kieSession01 = kieContainer.newKieSession("global-var-ksession");
        kieSession01.setGlobal("username", "zhangsan");
        // 全局变量可以用来接收规则执行后的结果
        kieSession01.setGlobal("logService", new LogService());
        kieSession01.fireAllRules();
        KieSession kieSession02 = kieContainer.newKieSession("global-var-ksession");
        // 全局变量可以用来接收规则执行后的结果
        kieSession02.setGlobal("logService", new LogService());
        kieSession02.fireAllRules();
        kieSession01.dispose();
        kieSession02.dispose();
    }
}
