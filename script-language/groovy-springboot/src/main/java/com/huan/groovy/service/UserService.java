package com.huan.groovy.service;

import com.huan.groovy.vo.AddUserRequest;

import java.io.IOException;
import java.net.URISyntaxException;

/**
 * 用户 service
 *
 * @author huan.fu
 * @date 2024/7/17 - 12:16
 */
public interface UserService {

    /**
     * 判断用户是否存在
     *
     * @param username 用户名
     * @return true: 存在 false: 不存在
     */
    boolean existUserName(String username);

    /**
     * 添加用户
     *
     * @param request 参数
     */
    String addUser(AddUserRequest request) throws URISyntaxException, IOException;
}
