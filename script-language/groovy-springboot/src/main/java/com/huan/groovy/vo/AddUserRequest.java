package com.huan.groovy.vo;

import lombok.Data;

/**
 * 添加用户请求参数
 *
 * @author huan.fu
 * @date 2024/7/17 - 12:17
 */
@Data
public class AddUserRequest {

    /**
     * 用户名
     */
    private String username;
    /**
     * 性别 0-女 1-男
     */
    private int sex;

    /**
     * 省
     */
    private String province;

    /**
     * 用户请求参数 验证脚本
     */
    private String validateScript;

    public static void main(String[] args) {
        String str = "package com.huan.groovy\n" +
                "\n" +
                "import com.huan.groovy.vo.AddUserRequest\n" +
                "\n" +
                "/**\n" +
                " *\n" +
                " * 验证脚本\n" +
                " * @author huan.fu\n" +
                " * @date 2024/7/17 - 12:37\n" +
                " */\n" +
                "class ValidateScript {\n" +
                "\n" +
                "    <T> T invoked(Map<Object, Object> content) {\n" +
                "        // 1、获取到请求参数\n" +
                "        AddUserRequest param = content[\"param\"]\n" +
                "        // 2、获取UserService类\n" +
                "        def userService = content[\"userService\"]\n" +
                "        // 3、进行参数验证\n" +
                "        if (userService.existUserName(param.getUsername())) {\n" +
                "            return \"用户名 \" + param.getUsername() + \" 已存在\"\n" +
                "        }\n" +
                "        if (param.getSex() != 0 || param.getSex() != 1) {\n" +
                "            return \"sex的值只可以是 0和1\"\n" +
                "        }\n" +
                "        return \"添加成功\"\n" +
                "    }\n" +
                "}\n";
    }
}
