package com.huan.groovy.controller;

import com.huan.groovy.service.UserService;
import com.huan.groovy.vo.AddUserRequest;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * groovy 控制器
 *
 * @author huan.fu
 * @date 2024/7/16 - 21:44
 */
@RestController
@RequestMapping("groovy")
public class GroovyController {

    @Resource
    private UserService userService;

    /**
     * 添加用户
     */
    @PostMapping("addUser")
    public String addUser(@RequestBody AddUserRequest request) throws URISyntaxException, IOException {
        return userService.addUser(request);
    }

}
