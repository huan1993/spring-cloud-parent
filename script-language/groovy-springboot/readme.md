# 需求

实现SpringBoot整合Groovy脚本，通过GroovyClassLoader动态加载groovy脚本。

[https://groovy-lang.org/integrating.html](https://groovy-lang.org/integrating.html)

# 执行脚本

```java
/**
 * 执行脚本
 *
 * @param script  脚本内容
 * @param context 上下文
 * @return T
 */
public<T> T exec(String script,Map<Object, Object> context){
        try{
        String fileName="Script_"+EncodingGroovyMethods.md5(script)+".groovy";
        GroovyCodeSource gcs=createCodeSource(()->new GroovyCodeSource(script,fileName,"/groovy/script"));
        Class<?> clazz=loader.parseClass(gcs,true);
        Object instance=clazz.newInstance();
        Method method=clazz.getMethod("invoked",Map.class);
        Object invoke=method.invoke(instance,context);
        return(T)invoke;
        }catch(Exception e){
        throw new RuntimeException(e);
        }
        }

private static GroovyCodeSource createCodeSource(PrivilegedAction<GroovyCodeSource> action){
        return java.security.AccessController.doPrivileged(action);
        }
```

# 内存泄漏

高版本的groovy目前暂未发现内存泄漏