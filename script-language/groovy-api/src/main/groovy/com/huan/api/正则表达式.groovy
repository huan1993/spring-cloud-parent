package com.huan.api

import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * 正则表达式
 * ^ $ . * + ? {} []字符集 [^]非字符集
 (ab)*,ab出现任意次数 (a|b)*,a或b出现任意次数
 元字符匹配要转义两个反斜杠\\ ,如:def reg =~ "\\.txt"
 斜杆/包裹正则只需要单个斜杆,如: def reg =~ /\.txt/,含义同上
 \w 单词字符, \d 数字, \s 空字符, \W 非单词字符, \D 非数字, \S 非空白字符
 * @author huan.fu
 * @date 2024/6/19 - 23:54
 */
class 正则表达式 {

    static void main(String[] args) {
        // 规则运算符 ～
        def p = ~/foo/  // 完全匹配 foo
        assert p instanceof Pattern
        println p.matcher('foo').matches()

        // 查找运算符 =～
        def text = "some text to match";
        // 使用 =～ 创建一个Matcher对象
        def m = text =~ /match/
        assert m instanceof Matcher
        println m.matches()

        // 匹配运算符 ==～
        m = text ==~ /match/
        println m
        assert m instanceof Boolean
    }

}
