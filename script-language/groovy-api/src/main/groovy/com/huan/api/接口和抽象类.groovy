package com.huan.api

import org.codehaus.groovy.util.StringUtil

/**
 *
 * @author huan.fu
 * @date 2024/6/22 - 23:51
 */
class 接口和抽象类 {

    static void main(String[] args) {
        // 阿里云发送短信
        SmsProvider smsProvider = new AliyunSmsProvider();
        smsProvider.send("手机号", "短信内容")
        // 模拟发送短信
        smsProvider = new MockSmsProvider();
        smsProvider.send("手机号", "短信内容")
    }

}

/**
 * 短信发送提供者
 */
interface SmsProvider {

    /**
     * 登录接口
     * @param phone 手机号
     * @param content 短信内容
     * @return true: 发送成功 false: 发送失败
     */
    boolean send(String phone, String content);
}

/**
 * 抽象的短信发送实现
 */
abstract class AbstractSmsProvider implements SmsProvider {

    @Override
    final boolean send(String phone, String content) {
        if (null == phone || "".equals(phone)) {
            return false;
        }
        return doSend(phone, content);
    }

    /**
     * 具体的发送短信实现
     * @param phone 手机号
     * @param content 短信内容
     * @return true: 发送成功 false: 发送失败
     */
    protected abstract boolean doSend(String phone, String content);
}

/**
 * 阿里云短信发送
 */
class AliyunSmsProvider extends AbstractSmsProvider {

    @Override
    protected boolean doSend(String phone, String content) {
        println "通过阿里云发送短信 手机号 $phone, $content"
        return true;
    }
}

/**
 * 模拟发送短信
 */
class MockSmsProvider extends AbstractSmsProvider {

    @Override
    protected boolean doSend(String phone, String content) {
        println "通过模拟发送短信 手机号 $phone, $content"
        return true;
    }
}