package com.huan.api

/**
 * groovy中的map,实际上就是java中的LinkedHashMap
 * @author huan.fu
 * @date 2024/6/19 - 00:20
 */
class Map {

    static void main(String[] args) {
        def map = ["create": "新创建",
                   "paied" : "已支付"];
        assert map instanceof LinkedHashMap

        // 赋值
        map['create'] = "新创建-1"
        // 获取值
        println map['create']

        // 赋值
        map.paied = "已支付-1"
        // 获取值
        println map.paied

        // 动态key
        def back = 'back-key';
        // 注意这个里面的()
        map[(back)] = "退款"

        println map.containsKey('back-key')

    }
}
