package com.huan.api

/**
 * 类中的方法
 * @author huan.fu
 * @date 2024/6/20 - 12:53
 */
class 方法 {

    /**
     * 无法数的方法
     */
    def method01() {
        println "无参数的方法"
    }

    /**
     * 带参数的方法
     * @param name 姓名
     * @param age 年龄，存在默认值
     */
    def method02(String name, age = 10) {
        println("我的名字是 $name, 今年$age 岁")
        // 注意和上面一句的区别
        println("我的名字是 $name, 今年${age}岁")
    }

    /**
     * 带返回值的方法
     */
    def method03(){
        return "返回值的方法";
    }

    static void main(String[] args) {
        def cl = new 方法()
        cl.method01();
        cl.method02("张三");
        cl.method02("李四", 20);
        println cl.method03();
    }
}
