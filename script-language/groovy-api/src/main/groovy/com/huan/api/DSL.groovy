package com.huan.api

/**
 * dsl 的简单使用
 * @author huan.fu
 * @date 2024/6/23 - 17:19
 */
class DSL {
    static void main(String[] args) {
        MacBookDsl.build {
            cpu "Apple M1 Max"
            mem "32G"
            show()
        }
    }
}

class MacBookDsl {
    def cpu;
    def mem;

    def static build(closure) {
        MacBookDsl dsl = new MacBookDsl()
        // 在闭包(closure)上调用的任何方法都会被委托到 MacBookDsl 对象上
        closure.delegate = dsl
        closure()
    }

    def cpu(String cpu) {
        this.cpu = cpu
    }

    def mem(String mem) {
        this.mem = mem
    }

    def show() {
        println "macbook cpu:$cpu mem:$mem"
    }
}
