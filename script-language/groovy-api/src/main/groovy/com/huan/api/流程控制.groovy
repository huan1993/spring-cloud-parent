package com.huan.api

/**
 * if while for 流程控制
 * @author huan.fu
 * @date 2024/6/20 - 13:02
 */
class 流程控制 {

    /**
     * 测试if语句
     * @param name 参数
     */
    def ifInvoked(String name) {
        if (name == "张三") {
            println "传递的参数是张三"
        } else if (name == "李四") {
            println "传递的参数是李四"
        } else {
            println "传递的参数是$name"
        }
    }

    def forInvoked() {
        for (count in 1..10) {
            if (count < 3) {
                println "continue"
                continue
            }
            if (count > 8) {
                println "break"
                break
            }
            println count
        }

        for (element in ["张三", "李四"]) {
            println element
        }

        for (entry in ["create": "新创建", "archived": "归档"]) {
            println entry.getKey() + " : " + entry.getValue()
        }
    }

    def whileInvoked() {
        def i = 10
        while (i-- > 0) {
            if (i == 8) {
                continue
            }
            if (i == 3) {
                break
            }
            println i
        }
    }

    def switchInvoked() {
        def str = "aaa"
        switch (str) {
            case "a":
                println "a"
                break
            case "aa":
                println "aa"
                break
            case "aaa":
                println "aaa"
                break
            default:
                println("default")
                break
        }


    }

    static void main(String[] args) {
        流程控制 lc = new 流程控制()
        lc.ifInvoked("张三")
        lc.ifInvoked("李四")
        lc.ifInvoked("王五")
        lc.forInvoked()
        lc.whileInvoked()
        lc.switchInvoked()
    }
}
