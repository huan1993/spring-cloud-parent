package com.huan.api

/**
 * 继承
 * @author huan.fu
 * @date 2024/6/22 - 23:39
 */
class 继承 {

    static void main(String[] args) {
        Student student = new Student(1000, true);
        student.setName("张三");
        println student;
    }
}

class Person {
    private String name;

    public Person() {

    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }
}

class Student extends Person {

    private int studentId;
    private boolean man;

    Student(int studentId, boolean man) {
        this.studentId = studentId
        this.man = man
    }

    int getStudentId() {
        return studentId
    }

    void setStudentId(int studentId) {
        this.studentId = studentId
    }

    boolean getMan() {
        return man
    }

    void setMan(boolean man) {
        this.man = man
    }

    @Override
    public String toString() {
        return "Student{" +
                "studentId=" + studentId +
                ", man=" + man +
                ", name=" + name +
                '}';
    }
}