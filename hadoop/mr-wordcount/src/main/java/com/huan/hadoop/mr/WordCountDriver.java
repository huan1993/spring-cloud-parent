package com.huan.hadoop.mr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * MapReduce程序的客户端驱动类，主要是构建Job对象实例
 *
 * @author huan.fu
 * @date 2023/7/8 - 12:07
 */
public class WordCountDriver {

    public static void main(String[] args) throws Exception {
        // 构建配置对象
        Configuration configuration = new Configuration();
        // 构建Job对象实例 参数（配置对象，Job对象名称）
        Job job = Job.getInstance(configuration, "wordCont");
        // 设置mr程序运行的主类
        job.setJarByClass(WordCountDriver.class);
        // 设置mr程序运行的 mapper类型和reduce类型
        job.setMapperClass(WordCountMapper.class);
        job.setReducerClass(WordCountReducer.class);
        // 指定mapper阶段输出的kv数据类型
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(LongWritable.class);
        // 指定reduce阶段输出的kv数据类型，业务mr程序输出的最终类型
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(LongWritable.class);
        // 配置本例子中的输入数据路径和输出数据路径，默认输入输出组件为： TextInputFormat和TextOutputFormat
        FileInputFormat.setInputPaths(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));

        // 是指 reduce task的个数（会导致输出的文件个数和reduce task的个数一样）
        job.setNumReduceTasks(1);
        // 提交作业
        boolean flag = job.waitForCompletion(true);
        // 退出程序
        System.exit(flag ? 0 : 1);
    }
}
