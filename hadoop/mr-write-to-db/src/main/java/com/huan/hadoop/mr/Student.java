package com.huan.hadoop.mr;

import lombok.Getter;
import lombok.Setter;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.mapred.lib.db.DBWritable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * MapReduce 从文件中读取数据，封装程这个对象，并写入到数据库中
 *
 * @author huan.fu
 * @date 2023/7/16 - 10:20
 */
@Getter
@Setter
public class Student implements DBWritable, Writable {
    /**
     * 学生编号
     */
    private long studentId;
    /**
     * 学生姓名
     */
    private String studentName;

    @Override
    public void write(PreparedStatement ps) throws SQLException {
        ps.setLong(1, this.studentId);
        ps.setString(2, this.studentName);
    }

    @Override
    public void readFields(ResultSet rs) throws SQLException {
        // ignore
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeLong(this.studentId);
        out.writeUTF(this.studentName);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        this.studentId = in.readLong();
        this.studentName = in.readUTF();
    }

    @Override
    public String toString() {
        return this.studentId + "\t" + this.studentName;
    }
}
