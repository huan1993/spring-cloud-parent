package com.huan.study.mybatis;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.huan.study.mybatis.mappers")
public class SqlMarkingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SqlMarkingApplication.class, args);
    }

}
