# 背景
在生产环境，有时间我们出现了一个慢查询，但是我们只有一个sql语句，如果快速定位到底是程序中的那个Sql语句出现了问题呢？

# 解决方案
通过`mybatis plugin`的方式，进行拦截SQL，将sql对应的mapper id 的值追加到 sql 中，方便快速定位问题

# 实现效果

```sql
select * from customer where phone = 'aaaaa';/**com.huan.study.mybatis.mappers.CustomerMapper.findCustomer*/
insert into customer(phone,address) values ('12345','湖北');/**com.huan.study.mybatis.mappers.CustomerMapper.addCustomer*/
```
可以看到 sql 语句后面都追加了 `/** 具体的mapper id的值 */`

# 博客地址

[https://blog.csdn.net/fu_huo_1993/article/details/146228056](https://blog.csdn.net/fu_huo_1993/article/details/146228056)