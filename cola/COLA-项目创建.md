COLA 开源地址： https://github.com/alibaba/COLA

你可以按照以下步骤去使用COLA：

**第一步：安装 cola archetype**
下载cola-archetypes下的源码到本地，然后本地运行mvn install安装。

**第二步：安装 cola components**
下载cola-components下的源码到本地，然后本地运行mvn install安装。

**第三步：创建应用**
执行以下命令：

1. 一个是用来创建纯后端服务的archetype：cola-archetype-service。
2. 一个是用来创建adapter和后端服务一体的web应用archetype：cola-archetype-web。

```
mvn archetype:generate  -DgroupId=com.alibaba.demo -DartifactId=demoWeb 
-Dversion=1.0.0-SNAPSHOT -Dpackage=com.alibaba.demo 
-DarchetypeArtifactId=cola-framework-archetype-web 
-DarchetypeGroupId=com.alibaba.cola -DarchetypeVersion=4.1.0
```

原文链接：https://blog.csdn.net/significantfrank/article/details/110934799
