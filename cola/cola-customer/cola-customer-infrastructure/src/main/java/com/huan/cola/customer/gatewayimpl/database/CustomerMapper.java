package com.huan.cola.customer.gatewayimpl.database;

import com.huan.cola.customer.gatewayimpl.database.dataobject.CustomerDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author huan.fu
 * @date 2022/6/7 - 11:23
 */
@Mapper
public interface CustomerMapper {

    List<CustomerDO> findAll();

    int insert(CustomerDO customerDO);

    int existsPhone(@Param("phone") String phone);
}
