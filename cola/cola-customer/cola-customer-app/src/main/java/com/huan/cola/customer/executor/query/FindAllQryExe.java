package com.huan.cola.customer.executor.query;

import com.alibaba.cola.dto.MultiResponse;
import com.huan.cola.customer.convert.CustomerDTOConvert;
import com.huan.cola.customer.domain.gateway.CustomerGateway;
import com.huan.cola.customer.dto.data.CustomerDTO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 查询所有的用户
 *
 * @author huan.fu
 * @date 2022/6/7 - 11:34
 */
@Component
public class FindAllQryExe {

    @Resource
    private CustomerGateway customerGateway;

    public MultiResponse<CustomerDTO> execute() {
        List<CustomerDTO> customerDTOS = customerGateway.findAll()
                .stream()
                .map(CustomerDTOConvert::toCustomerDTO)
                .collect(Collectors.toList());
        return MultiResponse.of(customerDTOS);
    }
}
