create table activity
(
	id int auto_increment comment '主键',
	activity_name varchar(100) null comment '活动名称',
	constraint activity_pk
		primary key (id)
)
comment '活动表' engine = innodb character set = utf8;