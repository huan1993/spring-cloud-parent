package com.huan.study.sharding.mappers;

import com.huan.study.sharding.entity.Customer;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author huan.fu 2021/5/18 - 上午10:43
 */
public interface CustomerMapper {

    int addCustomer(@Param("phone") String phone, @Param("address") String address);

    Customer findCustomer(@Param("phone") String phone);

    int countCustomer();

    List<Customer> findCustomerPage(@Param("pageIndex") int pageIndex, @Param("pageSize") int pageSize, @Param("id") Long id);
}
