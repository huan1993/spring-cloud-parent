package com.huan.study.excel.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学生信息
 *
 * @author huan.fu
 * @date 2024/3/23 - 22:34
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentInfo {

    /**
     * 编号
     */
    private Long id;
    /**
     * 班级
     */
    private String className;
    /**
     * 学生姓名
     */
    private String studentName;
    /**
     * 年龄
     */
    private Integer age;
}
