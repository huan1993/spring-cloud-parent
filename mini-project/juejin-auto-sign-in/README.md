# 需求

完成在`掘金`网站的自动签到和抽奖功能，并发送邮件告知结果。

需要修改 `spring.mail.password` 为对应的授权码 

# 当POM不是继承spring-boot-starter-parent的时候打包

```xml
<build>
  <!-- 打包后的名字 -->
  <finalName>juejin-auto-sign-in-0.0.1-SNAPSHOT</finalName>
  <plugins>
    <plugin>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-maven-plugin</artifactId>
      <version>2.5.6</version>
      <configuration>
        <mainClass>com.huan.juejin.JuejinApplication</mainClass>
        <layout>ZIP</layout>
      </configuration>
      <executions>
        <execution>
          <goals>
            <goal>repackage</goal>
          </goals>
        </execution>
      </executions>
    </plugin>
  </plugins>
</build>
```