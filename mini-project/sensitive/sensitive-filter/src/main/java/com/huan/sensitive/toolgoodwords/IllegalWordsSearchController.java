package com.huan.sensitive.toolgoodwords;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import toolgood.words.IllegalWordsSearch;
import toolgood.words.IllegalWordsSearchResult;

import javax.annotation.PostConstruct;
import java.util.Arrays;

/**
 * 一款高性能非法词(敏感词)检测组件，附带繁体简体互换，支持全角半角互换，获取拼音首字母，获取拼音字母，拼音模糊搜索等功能。
 * <a href="https://github.com/toolgood/ToolGood.Words">toolgood-words</a>
 *
 * @author huan.fu
 * @date 2024/1/21 - 20:48
 */
@RestController
@Slf4j
@RequestMapping("illegalWordsSearch")
public class IllegalWordsSearchController {

    private static final IllegalWordsSearch WORDS_SEARCH = new IllegalWordsSearch();

    @PostConstruct
    public void initSensitiveWord() {
        WORDS_SEARCH.SetKeywords(Arrays.asList("小[学", "放假", "小学生"));
    }

    @GetMapping("findFirst")
    public IllegalWordsSearchResult findFirst() {
        return WORDS_SEARCH.FindFirst("今天小学生放假了");
    }
}
