package com.huan.sensitive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 敏感词应用
 *
 * @author huan.fu
 * @date 2024/1/21 - 11:00
 */
@SpringBootApplication
public class SensitiveApplication {

    public static void main(String[] args) {
        SpringApplication.run(SensitiveApplication.class, args);
    }
}
