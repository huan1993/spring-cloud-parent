package com.huan.study.prometheus.histogram;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author huan.fu 2021/11/17 - 下午5:37
 */
@SpringBootApplication
public class PrometheusHistogramApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(PrometheusHistogramApplication.class);
    }
}
