package com.huan.seata.mapper;

import com.huan.seata.entity.OrderDO;

/**
 * @author huan.fu 2021/9/16 - 下午2:34
 */
public interface OrderMapper {
    
    int createOrder(OrderDO order);
}
