# 1、手动回滚分布式事物

```java
class 手动回滚分布式事物{
    public void 部分代码() {
        if (RootContext.inGlobalTransaction()) {
            try {
                GlobalTransactionContext.reload(RootContext.getXID()).rollback();
            } catch (TransactionException e) {
                log.error("回滚分布式事物出现异常", e);
            }
        }
    }   
}
```

访问请求：

```shell script
 $ curl -X GET http://localhost:50017/rollbackTx\?accountId\=1\&amount\=10\&hasException\=false 
```
**结果：**  
1. 当账户服务的返回值 <= 5 时，事物回滚，账户服务不扣钱，订单服务不产生订单  
2. 当账户服务的返回值 > 5 时，账户服务扣钱，订单服务产生订单  

# 2、临时挂起分布式事物

```java

class 临时挂起分布式事物 {
    
    @GlobalTransactional(rollbackFor = Exception.class)
    public void 部分代码(){
        String xid = RootContext.getXID();
        System.out.println("createAccountOrder:" + xid);
        // 解除 xid 的绑定
        RootContext.unbind();
        // 1、远程扣减账户余额
        boolean debitResult = remoteDebit(accountId, amount);
        // 重新绑定 xid
        RootContext.bind(xid);
        // 2、下订单
        orderService.createOrder(accountId, amount);
        // 抛出异常
        int i = 1 / 0;
    }
}
```

访问请求：

```shell script
 $ curl -X GET http://localhost:50017/bindAndUnBind\?accountId\=1\&amount\=10\&hasException\=false 
```

**结果：**  账户服务扣钱，没有产生订单

# 3、参考链接
http://seata.io/zh-cn/blog/seata-spring-boot-aop-aspectj.html