package com.huan.seata.controller;

import com.huan.seata.service.BusinessService;
import io.seata.core.context.RootContext;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author huan.fu 2021/9/16 - 下午2:45
 */
@RestController
@RequiredArgsConstructor
public class OrderController {
    
    private final BusinessService businessService;
    
    /**
     * 手动回滚分布式事物
     * 1、如果账户服务的返回值 <=0 则需要回滚分布式事物
     */
    @GetMapping("rollbackTx")
    public String rollbackTx(@RequestParam("accountId") Integer accountId, @RequestParam("amount") Long amount,
                             @RequestParam(value = "hasException", defaultValue = "false") boolean hasException) {
        System.out.println("createOrder:" + RootContext.getXID());
        businessService.rollbackTx(accountId, amount, hasException);
        return "下单成功";
    }
    
    /**
     * 在一个大的分布式事物中
     * 账户服务 不参与分布式事物
     * 订单服务 参与分布式事物
     */
    @GetMapping("bindAndUnBind")
    public String bindAndUnBind(@RequestParam("accountId") Integer accountId, @RequestParam("amount") Long amount) {
        System.out.println("createOrder:" + RootContext.getXID());
        businessService.bindAndUnBind(accountId, amount);
        return "下单成功";
    }
}
