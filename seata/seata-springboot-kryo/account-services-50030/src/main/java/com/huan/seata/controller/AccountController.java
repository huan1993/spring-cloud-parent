package com.huan.seata.controller;

import com.huan.seata.service.AccountService;
import io.seata.core.context.RootContext;
import io.seata.spring.annotation.GlobalTransactional;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * 账户控制器
 *
 * @author huan.fu 2021/9/16 - 下午1:57
 */
@RestController
@RequiredArgsConstructor
public class AccountController {
    
    private final AccountService accountService;
    
    @GetMapping("account/debit")
    @GlobalTransactional(rollbackFor = Exception.class)
    public String debit(@RequestParam("id") Integer id, @RequestParam("amount") Long amount) {
        System.out.println("debit:" + RootContext.getXID());
        accountService.debit(id, amount, new Date());
        return "扣减账户余额成功";
    }
}
