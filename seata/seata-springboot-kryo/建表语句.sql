create table account_date(
    id int unsigned auto_increment primary key comment '主键',
    name varchar(20) comment '用户名',
    balance bigint comment '账户余额,单位分',
    create_time datetime comment '创建时间',
    update_time datetime comment  '修改时间'
) engine=InnoDB comment '账户表';
insert into account_date(id,name,balance,create_time,update_time) values (1,'张三',100000,now(),now());

CREATE TABLE IF NOT EXISTS `undo_log`
(
    `branch_id`     BIGINT       NOT NULL COMMENT 'branch transaction id',
    `xid`           VARCHAR(128) NOT NULL COMMENT 'global transaction id',
    `context`       VARCHAR(128) NOT NULL COMMENT 'undo_log context,such as serialization',
    `rollback_info` LONGBLOB     NOT NULL COMMENT 'rollback info',
    `log_status`    INT(11)      NOT NULL COMMENT '0:normal status,1:defense status',
    `log_created`   DATETIME(6)  NOT NULL COMMENT 'create datetime',
    `log_modified`  DATETIME(6)  NOT NULL COMMENT 'modify datetime',
    UNIQUE KEY `ux_undo_log` (`xid`, `branch_id`)
) ENGINE = InnoDB COMMENT ='AT transaction mode undo table';