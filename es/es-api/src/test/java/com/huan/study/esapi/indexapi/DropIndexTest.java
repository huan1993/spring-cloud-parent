package com.huan.study.esapi.indexapi;

import com.huan.study.esapi.AbstractEsApi;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

/**
 * 删除索引
 * <a href="https://www.elastic.co/guide/en/elasticsearch/client/java-rest/7.12/java-rest-high-delete-index.html">删除索引</a>
 *
 * @author huan.fu 2021/5/7 - 上午9:56
 */
public class DropIndexTest extends AbstractEsApi {

    @DisplayName("删除索引")
    @Test
    public void dropIndex() throws IOException {
        // 删除索引，索引的名字
        DeleteIndexRequest request = new DeleteIndexRequest("test_index_001");
        // 设置超时时间
        request.timeout(TimeValue.timeValueSeconds(5));

        System.out.println("request: " + request);
        // 执行删除操作
        AcknowledgedResponse response = client.indices().delete(request, RequestOptions.DEFAULT);

        System.out.println("response: " + response);
    }
}
