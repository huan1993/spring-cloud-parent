package com.huan.es8.runtimefield;

import co.elastic.clients.elasticsearch._types.ScriptLanguage;
import co.elastic.clients.elasticsearch._types.mapping.RuntimeFieldType;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;

/**
 * 通过 runtime field 重写已有的字段， 即runtime field字段的名字和已有的字段名字一致
 * 注意返回值中 _source 中 lineName 和 fields中的 lineName 的区别
 *
 * @author huan.fu
 * @date 2023/2/1 - 23:08
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class OverrideFieldByRuntimeField extends AbstractRuntimeField {
    @Test
    @DisplayName("通过 runtime field 重写已有的字段， 即runtime field字段的名字和已有的字段名字一致")
    public void test01() throws IOException {
        SearchRequest request = SearchRequest.of(searchRequest ->
                searchRequest.index(INDEX_NAME)
                        // 查询所有数据
                        .query(query -> query.matchAll(matchAll -> matchAll))
                        // runtime field字段不会出现在 _source中，需要使用使用 fields api来获取
                        .fields(fields -> fields.field("lineName"))
                        // 创建一个 runtime filed 字段类型是 keyword
                        .runtimeMappings("lineName", runtime ->
                                runtime
                                        // 此处给字段类型为keyword
                                        .type(RuntimeFieldType.Keyword)
                                        .script(script ->
                                                script.inline(inline ->
                                                        // runtime field中如果使用 painless脚本语言，需要使用emit
                                                        inline.lang(ScriptLanguage.Painless)
                                                                .source("emit(params['_source']['lineName']+'new')")
                                                )
                                        )
                        )
                        // 进行聚合操作
                        .aggregations("agg_line_name", agg ->
                                // 此处的 aggLineName即为上一步runtime field的字段
                                agg.terms(terms -> terms.field("lineName").size(10))
                        )
                        .size(100)
        );

        System.out.println("request: " + request);
        SearchResponse<Object> response = client.search(request, Object.class);
        System.out.println("response: " + response);
    }
}
